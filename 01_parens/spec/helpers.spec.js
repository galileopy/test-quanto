const {
  isEmpty,
  contains,
  makeMatcher

} = require('../lib/helpers')

describe('check for helper functions', () => {
  it('can detect when a collection is empty', () => {
    expect(isEmpty('')).toBe(true)
    expect(isEmpty('hola')).toBe(false)
    expect(isEmpty([])).toBe(true)
    expect(isEmpty([1])).toBe(false)
    expect(isEmpty([1, 2])).toBe(false)
    expect(isEmpty(['hola'])).toBe(false)
  })

  it('finds when an element is part of a collection', () => {
    const evens  = [2, 4, 6, 8]
    const vowels  = 'aeiou'.split('')

    const isVowel = contains(vowels)
    const isEven = contains(evens)
    expect(isEven(2)).toBe(true)
    expect(isEven(3)).toBe(false)

    expect(isVowel('a')).toBe(true)
    expect(isVowel('e')).toBe(true)
    expect(isVowel('i')).toBe(true)
    expect(isVowel('o')).toBe(true)
    expect(isVowel('u')).toBe(true)

    expect(isVowel('z')).toBe(false)
    expect(isVowel('z')).toBe(false)
  })

  it('can match elements of a pair', () => {
    const pairs = ['12', '23', '34']
    const isNext = makeMatcher(pairs)

    expect(isNext('1', '2')).toBe(true)
    expect(isNext('2', '3')).toBe(true)
    expect(isNext('3', '4')).toBe(true)

    expect(isNext('0', '2')).toBe(false)
    expect(isNext('1', '3')).toBe(false)
    expect(isNext('2', '4')).toBe(false)
  })
})
