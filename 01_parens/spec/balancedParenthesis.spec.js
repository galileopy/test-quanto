const {
  balancedParenthesis,
  isMatch,
  isClosing,
  isOpening

} = require('../lib/balancedParenthesis')
describe('parenthesis balancing checker', () => {
  const balanced = '[()]{}{[()()]()}'
  const notBalanced = '[(])'

  it('can detect when a paren is not balanced', () => {
    const result = balancedParenthesis(notBalanced)
    expect(result).toBe(false)
  })

  it('can detect when a paren is balanced', () => {
    const result = balancedParenthesis(balanced)
    expect(result).toBe(true)
  })

  it('can find when there is a match', () => {
    expect(isMatch('[', ']')).toBe(true)
    expect(isMatch('{', '}')).toBe(true)
    expect(isMatch('(', ')')).toBe(true)

    expect(isMatch(']', '[')).toBe(false)
    expect(isMatch('}', '{')).toBe(false)
    expect(isMatch(')', '(')).toBe(false)
  })

  it('properly clasifies opening braces', () => {
    expect(isOpening('[')).toBe(true)
    expect(isOpening('{')).toBe(true)
    expect(isOpening('(')).toBe(true)

    expect(isOpening(']')).toBe(false)
    expect(isOpening('}')).toBe(false)
    expect(isOpening(')')).toBe(false)
  })

  it('properly clasifies closing braces', () => {
    expect(isClosing('[')).toBe(false)
    expect(isClosing('{')).toBe(false)
    expect(isClosing('(')).toBe(false)

    expect(isClosing(']')).toBe(true)
    expect(isClosing('}')).toBe(true)
    expect(isClosing(')')).toBe(true)
  })
})
