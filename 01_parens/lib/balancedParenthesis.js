// This is a basic stack problem.
// We can only process every element of the string once to make it O(n)
// If we limit ourself to using Array.prototype.pop, and Array.prototype.push
// then we have our stack.
// We have the freedom to have one stack per element since our space complexity
// is O(n^2)

const { isEmpty, contains, makeMatcher } = require('./helpers')

const pairs = [
  '{}',
  '()',
  '[]'
]

const opening = [ '(', '{', '[']
const closing = [ ')', '}', ']']


const isMatch = makeMatcher(pairs)
const isClosing = contains(closing)
const isOpening = contains(opening)


const balancedParenthesis = (expression) => {
  // A simple way to turn a string to an array
  const array = expression.split('')

  const resultingStack = array.reduce((stack, current) => {
    // if we just started, or the stack is clean
    if(isEmpty(stack)) {
      stack.push(current)
    }

    // if the current element is an opening element, then
    // we push it on the stack, and continue
    else if(isOpening(current)) {
      stack.push(current)
      return stack
    }

    // if the current element is a closing element, then it must close
    // the last element of the stack
    else if(isClosing(current)){
      const last = stack.pop()
      if(isMatch(last, current)){
        return stack
      }
      stack.push(last)
      stack.push(current)
    }
    return stack
  }, [])

  // if the resulting stack is empty, then we have a balanced expression
  // else, it wasnt balanced
  return isEmpty(resultingStack)
}

module.exports = {
  balancedParenthesis,
  isMatch,
  isClosing,
  isOpening
}
