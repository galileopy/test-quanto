const isEmpty = (arr) => arr.length === 0

const contains = collection => element =>  collection.includes(element)

const makeMatcher = pairs => (opening, closing) => {
  const pair = [opening, closing].join('')
  return pairs.includes(pair)
}


module.exports = {
  isEmpty,
  contains,
  makeMatcher
}
