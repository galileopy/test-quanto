const readline = require('readline')
const { balancedParenthesis } = require('./lib/balancedParenthesis')


const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
})

rl.on('line', line => {
  // eslint-disable-next-line no-console
  console.log(balancedParenthesis(line))
  rl.close()
})
