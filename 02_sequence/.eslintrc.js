module.exports = {
  "env": {
    "es6": true,
    "node": true
  },
  "extends": [
    "eslint:recommended",
    "node"
  ],
  "parserOptions": {
    "ecmaVersion": 2016
  },
  "rules": {
    "indent": [
      "error",
      2
    ],
    "import/no-commonjs": "off",
    "import/no-nodejs-modules": "off",
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "single"
    ],
    "semi": [
      "error",
      "never"
    ],
    "comma-dangle": ["error", {
      "arrays": "only-multiline",
      "objects": "only-multiline",
      "functions": "never"
    }]
  }
};
