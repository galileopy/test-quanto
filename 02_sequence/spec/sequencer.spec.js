const { readNumbers, sequencer } = require('../lib/sequencer')

describe('count and concat for numbers', () => {
  it('counts builds a series according to the rules of the problem', () => {
    expect(readNumbers(0)).toEqual(1)
    expect(readNumbers(1)).toEqual(11)
    expect(readNumbers(2)).toEqual(21)
    expect(readNumbers(3)).toEqual(1211)
    expect(readNumbers(4)).toEqual(111221)
  })

  it('takes the previous number of the series and returns the next', () => {
    expect(sequencer(1)).toEqual(11)
    expect(sequencer(11)).toEqual(21)
    expect(sequencer(21)).toEqual(1211)
    expect(sequencer(1211)).toEqual(111221)
    expect(sequencer(111221)).toEqual(312211)
    expect(sequencer(312211)).toEqual(13112221)
  })
})
