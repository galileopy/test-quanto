const { isEmpty, last, numberToArray } = require('../lib/helpers')

describe('helper functions', ()=> {
  it('turns a number into an array of numbers', () => {
    expect(numberToArray(1)).toEqual([1])
    expect(numberToArray(21)).toEqual([2, 1])
    expect(numberToArray(321)).toEqual([3, 2, 1])
  })

  it('gets the last element of an array', () => {
    expect(last([1])).toEqual(1)
    expect(last([1, 2])).toEqual(2)
    expect(last([[3, 4], [1, 2]])).toEqual([1, 2])
  })

  it('can detect when a collection is empty', () => {
    expect(isEmpty('')).toBe(true)
    expect(isEmpty('hola')).toBe(false)
    expect(isEmpty([])).toBe(true)
    expect(isEmpty([1])).toBe(false)
    expect(isEmpty([1, 2])).toBe(false)
    expect(isEmpty(['hola'])).toBe(false)
  })

})
