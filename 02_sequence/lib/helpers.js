const isEmpty = arr => arr.length === 0
const numberToArray = n => n
  .toString()
  .split('')
  .map(x => Number.parseInt(x, 10))

const last = arr => arr.slice(-1)[0]

module.exports = {
  isEmpty,
  numberToArray,
  last
}
