const { numberToArray, isEmpty, last } = require('./helpers')


const lastNumber = arr => last(last(arr))



// This is another not so common stack problem, but is still solvable,
// luckily we dont have a complexity constraint.

// We are going to use 2 simple data structures, a stack and a tuple
// the tuple will represent the count and number for every number we find
// The stack will help us have our results ordered. An the tuple will let us
// remember the last count and number.
// Over this structure, we want to have access
// * the last number
// * the last count
// * change last count,
//

const sequencer = (prev) => {
  const arr = numberToArray(prev)

  const result = arr.reduce((stack, number) => {
    // our structure is easy, as we find new numbers we add the count and the
    // number to the stack if the current number is already at the top of the stack
    // then we increment the counter and continue, at the end we flatten the stack
    // and join everything in one string, that then is converted to a number
    if(isEmpty(stack)) {
      stack.push([1, number])
    }

    else if(lastNumber(stack) === number){
      const [count, n] = stack.pop()
      stack.push([count + 1, n])
    }

    else { // we have another number, just push it
      stack.push([1, number])
    }

    return stack
  }, [])

  const string = result
    .reduce((acc, current) => acc.concat(current), [])
    .join('')

  return Number.parseInt(string, 10)
}

const readNumbers = (iterations) => {
  let prev = 1

  for(let i = 0; i < iterations; i++) {
    prev = sequencer(prev)
  }

  return prev
}

// count how many numbers are equal and contigous
// we're going to need tail and head
module.exports = {
  sequencer,
  readNumbers
}
